const io = require("socket.io")();
const jwt = require("jsonwebtoken");
var index = 0
var h = 0
var m = 1
var s = 0
var items = [
    {
        id: 1,
        name: "Laptop HP 15s-du1105TU",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop HP 15s-du1105TU 2Z6L3PA mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/1.jpg"
    },
    {
        id: 2,
        name: "Laptop Acer Aspire 3",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop Acer Aspire 3 mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/2.jpg"
    },
    {
        id: 3,
        name: "Laptop HP 240 G8",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop HP 240 G8 mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/3.jpg"
    },
    {
        id: 4,
        name: "Laptop HP 240 G8 342A3PA ",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop HP 240 G8 342A3PA mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/4.jpg"
    },
    {
        id: 5,
        name: "Laptop MSI Modern 14",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop MSI Modern 14 mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/5.jpg"
    },
    {
        id: 6,
        name: "Laptop Dell Inspiron 15",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop Dell Inspiron 15 mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/6.jpg"
    },
    {
        id: 7,
        name: "Laptop Acer Aspire 3",
        start_cost: 100,
        step_cost: 2,
        end_cost: 100,
        description: "Laptop Acer Aspire 3 mang đến vẻ bề ngoài thanh lịch và cực kỳ hiện đại với tông màu bạc sang trọng. Bên cạnh màu sắc đẹp mắt, laptop khiến người dùng ấn tượng bởi thiết kế mỏng và nhẹ mang đến tính di động hơn bao giờ hết.",
        log_deal: [],
        buyer:"",
        img:"/images/7.jpg"
    },
    
]
var allClients = [];
var currentItem = items[index];

function countDown() {
    s -= 1
    if (s === -1) {
        m -= 1
        s = 60
    }
    if (m === -1) {
        h -= 1
        m = 60
    }
    if (h == -1) {
        m = 0
        h = 0
        // clearTimeout(myTimer);
    }
    console.log(h, m, s);
    return [h, m, s]
}

// bộ đếm ngược thời gian
var myTimer = setInterval(() => {
    countDown()
}, 1000)

io.on("connection", function (socket) {
    // 1 client đã kết nối socket
    console.log("A user connected");
    try {
        // Trả về thông tin sản phẩm cho client
    socket.emit("display item", currentItem,items, index)

    // hiển thị lịch sử đấu giá
    socket.emit('log deal', currentItem.log_deal)

    // thêm 1 client vào db
    socket.on("new user", function (name) {
        if (name in allClients) {
            allClients[name] = socket;
        } else {
            console.log("A user add", name);
            socket.nickname = name;
            allClients[socket.nickname] = socket;
            informMess();
        }
    });

    // trả về client thời gian còn lại để đấu giá sp này
    socket.emit("timer",h,m,s)

    // nghe event hết giờ đấu giá
    socket.on('time out', (id)=>{
        // kiểm tra item hiện tại có đúng id từ client gửi ? 
        // => lưu thông tin đấu giá sp hiện tại , chuyển sang sp tiếp theo, reset bộ đếm
        socket.emit("chot deal",currentItem.buyer, currentItem.name, currentItem.end_cost)
        // if(!items[index++]){
        //     socket.emit("het san pham dau gia")
        //     return currentItem = null
        // }
        setTimeout(()=>{
            if(!id){
                console.log("id khong ton tai");
            }else{
                if(id == currentItem.id){
                    items[index] = currentItem
                    console.log(currentItem);
                    index++
                    currentItem = items[index]
                    console.log("oke");
                }else{
                    // nếu khác thì k làm gì
                    console.log("khác id");
                    return
                }
            }
        },1500)
        
    })



    // user trả giá
    socket.on("deal", function (deal_cost, nickname) {
        // kiểm tra giá mới >= giá cũ + bước giá
        if (currentItem.end_cost + currentItem.step_cost <= parseInt(deal_cost)) {
            currentItem.end_cost = parseInt(deal_cost);
            let time = new Date().toLocaleString();
            let deal = { nickname: nickname, time: time, deal_cost: parseInt(deal_cost) };
            currentItem.buyer = nickname;
            currentItem.log_deal.push(deal)
            io.emit("update deal", parseInt(deal_cost), nickname, time, currentItem.end_cost);
        }
    });


    // thông báo đầu tiên cho người tham gia
    function informMess() {
        socket.emit(
            "inform message",
            Object.keys(allClients),
            currentItem,
            // currentItem.end_at,

        );
    }

    } catch (error) {
        
    }
    


    // 1 user huỷ kết nối
    socket.on("disconnect", function (data) {
        if (!socket.nickname) return;
        delete allClients[socket.nickname];
    });
});

const socketApi = {
    io: io,
};

module.exports = socketApi;

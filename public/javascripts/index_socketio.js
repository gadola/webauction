
var socket = io();

var form_deal = document.querySelector('#form_deal')
var form_login = document.querySelector('#form_login')
var inp_username = document.querySelector('#username')
var inp_deal_cost = document.querySelector('#deal_cost')
var p_item_name = document.querySelector('#item_name')
var p_item_cost_start = document.querySelector('#item_cost_start')
var p_item_cost_last = document.querySelector('#item_cost_last')
var p_item_cost_step = document.querySelector('#item_cost_step')
var div_info = document.querySelector('#info')
var nickname = document.querySelector("#nickname")
var currentItem = {}
var currentUser = ''
var p_count_down = document.querySelector('#demnguoc')
var p_times = document.querySelector('#times')
const COUNT_DOWN_TIME = 60000
// form login
form_login.addEventListener('submit', (e) => {
    e.preventDefault()
    if (inp_username.value) {
        socket.emit('new user', inp_username.value)
        sessionStorage.setItem('nickname', inp_username.value)
        nickname.textContent = sessionStorage.getItem('nickname')
    } else {
        alert('Vui lòng nhập nickname của bạn!')
    }
    $('#form_login').hide()
    $('#form_deal').show()
})

// form deal
form_deal.addEventListener('submit', (e) => {
    e.preventDefault()
    let deal_cost = parseInt(form_deal.deal_cost.value)
    if (deal_cost >= currentItem.cost_start && deal_cost >= currentItem.cost_last + currentItem.cost_step) {
        socket.emit('deal item', deal_cost, sessionStorage.getItem('nickname'))
        form_deal.deal_cost.value = ""
    } else {
        alert("giá tiền không hợp lệ")
    }
})

// chốt deal
var time_chot_deal = setTimeout(()=>{
    console.log("Không có ai mua hết!");
    socket.emit('close the deal not buyer')
},COUNT_DOWN_TIME)

// in ra 1 lịch sử deal
function updateLogDeal(nickname, time, deal_cost) {
    let p = document.createElement('p')
    p.textContent = nickname + " trả giá là: " + deal_cost+ " , lúc " + time 
    div_info.appendChild(p)
}

// cập nhật giá sau deal
socket.on('update cost item', (deal_cost, nickname, time, end_at) => {
    currentItem.cost_last = deal_cost
    updateLogDeal(nickname, time, deal_cost)
    showDataItem()
    let end_at_str = new Date(end_at).toUTCString()
    p_count_down.textContent = `Chốt đơn lúc ${end_at_str}`
    
    clearTimeout(time_chot_deal) // RESET ĐẾM NGƯỢC
    time_chot_deal = setTimeout(()=>{
        console.log("chốt deal thành công");
        socket.emit('close the deal')
    },COUNT_DOWN_TIME)
})



// cập nhật thông tin khi mới login
socket.on('inform message', (user, order ) => {

    // lưu socket
    // sessionStorage.setItem('socket',socket)

    console.log(user);
    console.log(order);
    let end_at = new Date(order.end_at).toUTCString()
    p_count_down.textContent = `Chốt đơn lúc ${end_at}`
    currentItem.log_deal = order.log_deal
    currentItem.name = order.item.name
    currentItem.cost_start = parseInt(order.item.cost_start)
    currentItem.cost_last = parseInt(order.item.cost_last)
    currentItem.cost_step = parseInt(order.cost_step)

    showDataItem()

    div_info.innerHTML = ''
    // hiển thị toàn bộ lịch xử deal
    order.log_deal.forEach(data => {
        updateLogDeal(data.nickname, data.time, data.deal_cost)
    });
})

// cập nhật thông tin vật phẩm
function showDataItem() {
    p_item_name.textContent = currentItem.name
    p_item_cost_start.textContent = currentItem.cost_start
    p_item_cost_last.textContent = currentItem.cost_last
    p_item_cost_step.textContent = currentItem.cost_step
}

// đồng hồ
var start = Date.now();
setInterval(function() {
    p_times.textContent = new Date().toUTCString()
}, 1000);



// thông báo chốt deal
socket.on('inform buyer', (order) => {
    if(order.buyer){
        alert(`chúc mừng bạn ${order.buyer} đã mua với giá ${order.item.cost_last} `)
        location.reload();
        
    }else{
        
        alert(`không có ai mua vật phẩm này hết`)
        location.reload();
    }
})